#include <stdio.h>
#include <stdlib.h>

//Creamos una estructura llamada nodo que contenga un entero ("inf") y un puntero ("sig")

struct nodo {
    int info;
    struct nodo *sig;
};

//Inicializamos una variable global llamada raiz y le asignamos NULL (osea se inicializa en nada)
struct nodo *raiz = NULL;

//Insertar recibe como parametro un entero y lo inserta ala stack

void insertar(int x) {
    //se crea una variable local llamada nuevo de tipo puntero
    struct nodo *nuevo;
    //Se le asigna un espacio de memoria suficiente como para almacenar la estructura nodo
    nuevo = malloc(sizeof (struct nodo));
    //Como nuevo es una estructura podemos acceder a sus propiedades con una arrow fuction y le asignamos la variable x que es el parámetro del la función
    nuevo->info = x;
    //Verificamos si raiz (la variable global) es nula (la primera vez será cierta)
    if (raiz == NULL) {
        //Si es nula, entonces a raiz se le asigna el puntero nuevo
        raiz = nuevo;
        //sig que es un puntero de la estructura nodo se le asigna null
        nuevo->sig == NULL;
        //si no es nulo entonces...
    } else {
        //El puntero de la estructura nodo (sig) se le asigna el puntero raiz
        nuevo->sig = raiz;
        // y a raíz se le asigna el contenido de nuevo
        raiz = nuevo;
    }
}

//nos dice los valores dentro de la lista

void imprimir() {
    //creamos una variable local llamada reco que es de la estructura nodo y le asignamos el valor de raiz
    struct nodo *reco = raiz;
    //Se imprimen los valores
    printf("Lista completa:\n");
    while (reco != NULL) {
        printf("%i ", reco->info);
        reco = reco->sig;
    }
    printf("\n");
}

int extraer() {
    if (raiz != NULL) {
        int informacion = raiz->info;
        struct nodo *bor = raiz;
        raiz = raiz->sig;
        free(bor);
        return informacion;
    } else {
        return -1;
    }
}

void liberar() {
    while(extraer()!=-1){
        extraer();
    }
}
//--------------------------------------------------------------------------------------------------------------


//Nos dirá si la stack está vacia o no

void estaVacia() {
    //Evalua si raiz sigue siendo null o su valor ya ha cambiado
    if (raiz == NULL) {
        printf("La pila está vacía\n");
    } else {
        printf("La pila no está vacía\n");
    }
}

//Nos dirá cuantos nodos tiene la stack 

int cantidad() {
    //Iniciamos con que la cantidad es de 0
    int cant = 0;
    //Iniciamos un puntero de la estructura nodo y le asignamos el valor de raiz
    struct nodo *reco = raiz;
    //Si reco que tiene asignada a rais es diferente de null entrará en el bucle while
    while (reco != NULL) {
        //La cantidad amentará en la unidad
        cant++;
        //reco se le asigna el puntero al que apunta despues (sig)
        reco = reco->sig;
    }
    //Una vez finalizado el bucle while nos dirá cuanto es el valor entero de cant
    return cant;
}

int main() {
    //flag para saber cuanto se reservará en memoria
    printf("sizeof: %i\n", sizeof (struct nodo));
    //mandamos a llamar a la función estaVacia() para saber si la stack tiene valores o no (tendría que devolver que no porque no hemos insertadp nada)
    estaVacia();
    //Hacemos la inserción de valores
    insertar(20);
    insertar(43);
    insertar(7);
    //Imprimimos los valores de la stack
    imprimir();
    //Otro flag para saber si la stack está vacio o no (tendría que decir que no esta vacis)
    estaVacia();
    //Imprimirá la cantidad de nodos que halla
    printf("Número de nodos: %i\n", cantidad());
    //Nos dirá que valor saldrá (tendrá que ser el último en ingresar
    printf("Extraemos de la pila: %i\n", extraer());
    //Imprimimos los valores de la stack
    imprimir();
    //Liberará todo el espacio en memoria quitando los valores de la stack
    liberar();
    //Imprimimos los valores de la stack (para ver que este vacia)
    imprimir();
    //Imprimirá la cantidad de nodos 
    printf("Número de nodos: %i\n", cantidad());
    return 0;
}
