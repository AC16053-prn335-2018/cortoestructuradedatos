#include <stdio.h>
#include <stdlib.h>

//Creamos una estructura llamada nodo que contenga un entero ("inf") y un puntero ("sig")

struct nodo {
    int info;
    struct nodo *sig;
};

//Inicializamos una variable global llamada raiz y le asignamos NULL (osea se inicializa en nada)
struct nodo *raiz = NULL;

void insertar(int x) {
    //se crea una variable local llamada nuevo de tipo puntero
    struct nodo *nuevo;
    //Se le asigna un espacio de memoria suficiente como para almacenar la estructura nodo
    nuevo = malloc(sizeof (struct nodo));
    //Como nuevo es una estructura podemos acceder a sus propiedades con una arrow fuction y le asignamos la variable x que es el parámetro del la función
    nuevo->info = x;
    //Verificamos si raiz (la variable global) es nula (la primera vez será cierta)
    if (raiz == NULL) {
        //Si es nula, entonces a raiz se le asigna el puntero nuevo
        raiz = nuevo;
        //sig que es un puntero de la estructura nodo se le asigna null
        nuevo->sig == NULL;
        //si no es nulo entonces...
    } else {
        //El puntero de la estructura nodo (sig) se le asigna el puntero raiz
        nuevo->sig = raiz;
        // y a raíz se le asigna el contenido de nuevo
        raiz = nuevo;
    }
}

//nos dice los valores dentro de la lista

void imprimir() {
    //creamos una variable local llamada reco que es de la estructura nodo y le asignamos el valor de raiz
    struct nodo *reco = raiz;
    //Se imprimen los valores
    printf("Lista completa:\n");
    while (reco != NULL) {
        printf("%i ", reco->info);
        reco = reco->sig;
    }
    printf("\n");
}


void reemplazar(struct nodo *pila, int viejo, int nuevo) {
    //Evalua si pila es nula
    if (pila != NULL) {
        //Evalua si la variable info del nodo actual es igual (machea) con el valor antiguo
        if (pila->info == viejo) {
            //De ser cierto entonces lo reemplaza 
            pila->info = nuevo;
        } else {
            //De no ser cierto entonces ocupamos recursividad para pasar al siguiente nodo 
            reemplazar(pila->sig, viejo, nuevo);
        }
    } else {
        //Si ya pasó por todos los nodos y no encontró ningún valor que machee la variable viejo entonces imprime que no se encontró
        printf("El valor viejo no se encuentra en la pila\n");
    }
}

int main() {
    //Insertamos valores en la pila
    insertar(10);
    insertar(20);
    insertar(30);
    //Le mostramos los valores al usuario
    imprimir();
    
    //iniciamos una variable que almacene a viejo y nuevo
    int viejo;
    int nuevo;
    
    printf("¿Qué valor de la pila quiere reemplazar?\n");
    scanf("%d", &viejo);    
    printf("¿Qué valor quiere asignar?\n");
    scanf("%d", &nuevo);
    reemplazar(raiz, viejo, nuevo);
    //Imprimimos su cambio para que se vea reflejado
    imprimir();
    return 0;
}