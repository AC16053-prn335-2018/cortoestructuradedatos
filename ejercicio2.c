#include <stdio.h>
#include <stdlib.h>
//Hay que importar esta libreria para poder ocupar algunas funciones más adelante :B
#include <string.h>

//Quize de una vez que funcionara con todo por eso lo hice como char y no como int

//Creamos la estructura que será el nodo

struct nodo {
    //En este caso ocuparemos char, no siempre es necesario ocupar int xD
    char dato;
    struct nodo *sig;
};

//Inicializamos en null una variable global llamada raiz
struct nodo *raiz = NULL;

//insertaremos un char a travez de esta función
void insertar(char x) {
    //Creamos una variable local de la estructura nodo y será un puntero
    struct nodo *nuevo;
    //La inicializamos y le cedemos un espacio de memoria sificiente como el tamaño de la estructura
    nuevo = malloc(sizeof (struct nodo));
    //Le asignamos el parámetro "x" a lapropiedad dato de el puntero nuevo 
    nuevo->dato = x;
    //Verifica si raiz (el puntero) sigue siendo nulo, porque la primera vez lo será
    if (raiz == NULL) {
        //De ser así hacemos que el puntero raíz apunte hacia el puntero nuevo
        raiz = nuevo;
        //Le asignamos un valor nulo a la propiedad sig del puntero nuevo
        nuevo->sig == NULL;
        //De no ser nulo...
    } else {
        //Le asifnamos el puntero raíz a la propiedad sig(que tambien es un puntero)
        nuevo->sig = raiz;
        //Apuntamos raiz adonde apunta nuevo
        raiz = nuevo;
    }
}

//Imprimiremos los datos a travez de esta función
void imprimir() {
    //Creamos una variable y le asignamos el valor de raiz
    struct nodo *reco = raiz;
    //Imprimimos en consola el numero invertido
    printf("El número invertido es: \n");
    //mientras que no sea null imprimirá los valores dentro de el
    while (reco != NULL) {
        printf("%c", reco->dato);
        reco = reco->sig;
    }
    printf("\n");
}

void main() {
    //Inicializamos una variable que es un vector de tipo char para que se puedan ingresar letras o caracteres
    char dato[40];
    //Imprimimos para que el usuario sepa que hacer
    printf("Ingrese un número:\n");
    //Escaneamos lo que el usuario ingresó y como queremos que llene los espacios necesarios no lo pasamos por referencia
    scanf("%s", dato);
    //Variable local que sirve para el bucle for
    int i;
    //La funcion de strlen() es para contar los valores que se ocupan dentro del array de chars (para poderse utilizar tuvimos que añadir la libreria)
    for (i = 0; i < strlen(dato); i++) {
        //Comenzamos ainsertar dato por dato
        insertar(dato[i]);
    }
    //Imprimimos los valores
    imprimir();
}
